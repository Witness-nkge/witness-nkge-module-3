import 'package:flutter/material.dart';

class FeatureOne extends StatelessWidget {
  const FeatureOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Feature Screen 1"),
        ),
      ),
    );
  }
}