import 'package:flutter/material.dart';
import 'package:flutter_app/edit.dart';
import 'package:flutter_app/feature_screen_one.dart';
import 'package:flutter_app/feature_screen_two.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(),
            ),
            Padding(padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => FeatureOne()))
                },
                child: const Text("feature screen 1"),
              ),
            ),
            Padding(padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => FeatureTwo()))
                },
                child: const Text("feature screen 2"),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => {
            Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile()))
          },
          child: const Icon(Icons.edit),
        ),
      ),
    );
  }
}