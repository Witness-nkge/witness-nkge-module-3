import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/edit.dart';
import 'package:flutter_app/login.dart';
import 'package:flutter_app/main.dart';

class Second extends StatelessWidget {
  const Second({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home : Scaffold(
        appBar: AppBar (
          title: const Text("Profile"),
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Name"
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Username"
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email"
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Password"
                ),
              ),
            ),

            Padding(padding: const EdgeInsets.all(20.0),
            child: ElevatedButton(
              onPressed: () => {
                Navigator.pop(context, MaterialPageRoute(builder: (context) => First()))
              },
              child: const Text("register"),
            ),
            ),

            ],
        ),
      ),
    );
  }
}