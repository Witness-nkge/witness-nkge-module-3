import 'package:flutter/material.dart';
import 'package:flutter_app/dashboard.dart';
import 'package:flutter_app/edit.dart';
import 'package:flutter_app/register.dart';

class First extends StatelessWidget {
  const First({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home : Scaffold(
        appBar: AppBar (
          title: const Text("Profile"),
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email"
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Password"
                ),
              ),
            ),
            Padding(padding: const EdgeInsets.all(0.0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()))
                },
                child: const Text("login"),
              ),
            ),
            Padding(padding: const EdgeInsets.all(0.0),
              child: TextButton(
                onPressed: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Second()))
                },
                child: const Text("click here to register"),
              ),
            )
          ],
        ),
      ),
    );
  }
}