import 'package:flutter/material.dart';

class FeatureTwo extends StatelessWidget {
  const FeatureTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Feature Screen 2"),
        ),
      ),
    );
  }
}